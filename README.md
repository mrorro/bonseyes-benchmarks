# Bonseyes benchmark data

This repository contains a set of benchmark data performed with the Bonseyes LPDNN inference framework on a variety of heterogeneous embedded platforms. 

The benchmark process has the objective of profiling different metrics while AI Applications are being executed on such heterogeneous devices. The benchmark contains the following main metrics:

- Static metrics (not measured on device):
  - FLOPs
  - Parameters
  - Storage
-  Dynamic metrics (measured on device):
   - Latency
   - Throughput
   - Average Memory (CPU/GPU)
   - Peak Memory (CPU/GPU)
   - Memory Bandwidth (CPU/GPU)
   - Processor load (CPU/GPU/NPU)
   - Power consumption
   - Temperature 

**Find complete benchmark dataset under [results](results) and by platform under [platforms](platforms).**

## LPDNN inference framework

LPDNN, which stands for Low-Power Deep Neural Network, is an enabling deployment framework that provides the tools and capabilities to generate portable and efficient implementations of DNNs. The main goal of LPDNN is to provide a set of AI applications for deep learning tasks, e.g., object detection, image classification, speech recognition, which can be deployed and optimised across heterogeneous platforms, e.g., CPU, GPU, FPGA, DSP, NPU. 

![LPDNN AI classes](doc/LPDNN_AI_classes.jpg)

![LPDNN overview](doc/LPDNN_overview.png)

One of the main issues of deep learning systems is the hardship to replicate results across different systems. To solve this issue, LPDNN features a full development flow for deep learning solutions on embedded devices by providing platform support, sample models, optimisation tools, integration of external libraries and benchmarking. LPDNN’s full development flow makes the AI solution reliable and easy to replicate across systems. 

![LPDNN full stack](doc/LPDNN_full_stack.jpg)

### Inference engines

LPDNN's AI applications contain a hierarchical but flexible architecture that allows new modules to be integrated within the LPDNN framework through an extendable and straightforward API. For instance, LPDNN supports the integration of 3rd-party self-contained inference engines to perform DNN inference. Currently, these are LPDNN's supported inference engines:

- **LNE**: [LPDNN Native Engine](https://arxiv.org/abs/1901.05049) (LNE) allows the execution of DNNs across arm-based and x86 CPUs as well as on Nvidia-based GPUs.
- **NCNN**: [NCNN](https://github.com/Tencent/ncnn) ports the execution of DNNs on GPU through the Vulkan driver. 
- **TensorRT**: [TensorRT](https://developer.nvidia.com/tensorrt) accelerates the DNN inference on Nvidia-based GPUs.
- **ONNXruntime**: [ONNXruntime](https://github.com/microsoft/onnxruntime) enables the direct execution of ONNX models on CPUs and GPUs.

The inclusion of external engines also benefits LPDNN as certain embedded platforms provide their own specific and optimised framework to deploy DNNs on their hardware.

### Heterogeneous computing support

One of the main factors for LPDNN’s adoption is performance portability across the wide span of hardware platforms. LPDNN's flexible architecture allows the main core to remain small and dependency-free while additional 3rd party libraries or inference engines are only included when needed and for specific platforms, notably increasing the portability across systems. Besides, cross-compilation and specific tools are added to support a wide range of heterogeneous computing platforms such as CPUs, GPUs, ASICs. One of the objectives of LPDNN is to provide full support for reference platforms by providing:

- **Developer Platform Evironments (DPEs)** to help the user employ a developer platform, including OS images, drivers, and cross-compilation toolchains for several heterogeneous platforms.
- **A dockerised and stable environment**, which increases the reliability by encouraging the replication of results across platforms and environments.
- **Optimisation tools and computing libraries** for a variety of computing embedded platforms that can be used by LPDNN's inference engines to accelerate the execution of neural networks.


## Available embedded platforms

The range of embedded platforms that are currently benchmarked with LPDNN are the following:

- iMX8m Nano
- STM32 MP1
- Raspberry Pi 3b+
- Raspberry Pi 4b
- Nvidia Jetson Nano
- Nvidia Jetson Xavier
- Intel NUC

## Collection process

To carry out the collection dataset collection perform the following steps:
- Set up target hardware as explained in [Bonseyes Developer Platforms](https://beta.bonseyes.com/doc/pages/user_guides/platform_index.html).
- Download AI Application from the Bonseyes Marketplace as explained in [Download AI App from BMP](https://beta.bonseyes.com/doc/pages/user_guides/ai_app_index.html#lpdnn-ai-apps).
- Obtained LPDNN deployment package as explained in [Obtain deployment package from BMP](https://beta.bonseyes.com/doc/pages/user_guides/ai_app_index.html#obtain-an-lpdnn-deployment-package).

### Donwload AI-App from BMP
AI Apps can be donwloaded from the BMP by executing the following command (replace ${ai_app} by the name you would like to give to your AI-App):

```shell
    bonseyes marketplace download-ai-app --output-dir ${ai_app}
```

A dialog will prompts asking you to choose the AI-App to download:


![BMP AI App download](doc/BMP_AI-App_download.png)


The ai app will be downloaded in the directory ``${ai_app}`` 


### Download Deployment Package

Copy your LPDNN's platform-specific deployment pacakge in the same folder as the target platform and the AI-App. Next, decompress it (change ${deployment_package} by the name of your package):

```shell
    cp ${deployment_package}.tar.gz my-bonseyes-platform
    tar -xvf ${deployment_package}.tar.gz  
```
 
### Benchmark your AI-App you target HW

To benchmark an AI-App, you folder should contain the following elements:

- ${platformName_config}: target configuration coming from the DPE setup
- ${deployment_package}: downloaded from Gitlab
- ${ai_app}: downloaded from the BMP

To benchmark an AI app you need to execute the following command:

```shell
    bonseyes ai-app benchmark-analyzer --target-config ${platformName_config} \
                                       --ai-app ${ai_app} \
                                       --deployment-package ${deployment_package} \
                                       --dataset PATH/TO/DATASET_FOLDER \ 
                                       [--number 20] \
                                       [--filename ${FileName}]
```

For instance, to benchmark Squeezenet's AI-APp for Imagenet on the Jetson Nano platform looks like:

```shell
    bonseyes ai-app benchmark-analyzer --target-config jetson_nano-jetpack4.6_config \
                                       --ai-app squeezenet_v1 \
                                       --deployment-package jetson_nano-jetpack4.6 \
                                       --dataset /samples/ILSVRC2012 \
                                       --number 20 \
                                       [--filename benchmark.json]
```

This call will perform inference on twenty images from the ILSVRC2012 folder for squeezenet_v1 AI-App on the Jetson Nano platform with JetPack 4.6. 

To store the benchmark in a file, add the *--filename* option and the metrics will be dumped in the named file. If the file name option is enable, prediction results will be automatically dump into *results.txt**.


## Licensing and Contributing
This project is licensed under the terms and conditions of the MIT License. A copy of the license is accessible here: [LICENSE](LICENSE).  
  
Details on contributing to this project are found in the file [doc/CONTRIBUTING.md](doc/CONTRIBUTING.md).

### Disclaimer
Bonseyes Community Association provides only scripts for downloading and preprocessing public datasets. We do not own these datasets and are not responsible for their quality or maintenance.

Please ensure that you have the permission to use a dataset under the dataset's license. We are not a law firm and does not provide legal advice. Our open source tools are meant to be a jumping-off point, not a final word on whether your use is compatible with a particular license. For example, some licenses may allow commercial use, but still have requirements about attribution and other things. It is your responsibility alone to read the license and follow it.

Bonseyes Community Association provides this information and tools on an “as-is” basis without warranties of any kind, and disclaims liability for all damages resulting from your use of the license information and tools. Please seek the advice of a licensed legal professional in your jurisdiction if you have any questions about a particular license.
